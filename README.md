# Frontend Mentor - Chat app CSS illustration

![Design preview for the Chat app CSS illustration coding challenge](./design/desktop-preview.jpg)

## Links

- Live site: <https://fem-chat-app-landing.vercel.app/>
- Solution page: <https://www.frontendmentor.io/solutions/chat-app-css-illustration-rJNIxspS5>
